## READ.ME :
Sujet du projet :

Est-ce que la présence/absence d’un certain type de domaine fonctionnel peut déterminer l’appartenance à une sous-famille de domaine de transporteurs chez les procaryotes ?

# Données utilisées:
Pour réaliser ce projet, on se propose d'utiliser les tables suivantes :
  
- Functionnal_Domain : Cette table comprend les noms des différents domaines portés sur un gène. Parmi les variables, on a Family_link qui permet de faire le lien entre les types de domaines ABC et les sous-familles de transporteurs.
  
- Taxonomy : Cette table de données permet seulement de s'assurer que l'on ne travaille qu’avec des procaryotes et pas des eucaryotes.
  
- Protein et Assembly : Ces deux tables contiennent les Gene_ID des gènes qui sont potentiellement impliqués dans les transporteurs ABC. Ces tables permettent donc de faire le lien entre les domaines et les transporteurs. En effet, Assambly contient les différentes sous-famille de transporteurs qui nous interessent. La table Protein contient différentes variables que l'on utilise pour nettoyer nos données. 

- Gene : On utilise cette table pour "nommer" nos individus dans la matrice finale à l'aide de l'identifiant des gènes. 

- On utilise les tables Conserved_Domain, Chromosome, Strain pour joindre nos 4 tables d'intérêts.

# Filtrage des données :
Ces données comportent pas mal d'erreur, pour filter les données, on utilise dans la table Protein :
- Identification_Status : on garde les domaines validés par les experts.
- Type : on séléctionne uniquement le type 'ABC'.
- Integrity : on récupere les protéines qui ne sont pas partielles

Ensuite, dans la table Conserved_Domain, on filtre sur la variable e_Value. Les domaines qui ont une e-value supérieur à 0.02 ne sont pas gardés.
Pour s'assurer que les gènes sont issus d'organismes non eucaryotes, on fait un subset de la matrice en recherchant "Eukaryota" dans Taxon_Name.
# Chargement des données et packages :

Ces tables sont des fichiers au format .tsv chargés  à partir de la base de données de MySQL (Mariadb SOUS Linux) avec la fonction dbConnect() et MySQL(). Le traitement de ces données est rendu possible par les packages R tel que : 
RMySQL : permet de se connecter à une base de données MySQL dans R, d’importer un tableau de la base de données dans R.
DBI : définit une interface commune entre R et les systèmes de gestion de base de données (SGBD) qu'on utilise.


# Conception de la matrice individus-variables :

Nous voulons prédire  le type de sous-famille de transporteur en fonction de la présence ou l'absence des différents domaines caractérisant un transporteur type ABC( MSD, SBP, NBD), ainsi, notre variable "classe" sera Functionnal_Classification dans la table Assembly.
Pour construire la matrice individus-variables, on a récupéré les variables qui nous intéressent notamment Gene_ID (pour représenter les individus), Domain_Organization, Taxon_Name, Gene_ID, Functional_Classification. Nous avons pu fusionner nos variables dans des tables distinctes en prenant en compte les variables de “lien” entre les tables deux à deux. Le but du script R est de créer une variable qui indique quels domaines sont présents dans le transporteurs en fonction de Domain_Organization. Pour cela, on récupere les positions (dans le tableau) des domaines lorsqu'il sont présent, ensemble ou pas et on remplit un vecteur avec le nom du/des domaine-s en question. On obtient un vecteur qui pour chaque ligne indique les domaines présents dans le transporteur. Ce vecteur est la variable Domain_Class que l'on va diviser en 3 variables quantitatives pour chacun des 3 domaines qui indique présence ou absence.


