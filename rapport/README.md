# I) Contexte

La famille des transporteurs à ATP binding cassette (ABC) ou systèmes ABC font partie des familles de protéines les plus importantes en termes de taille et d’implication dans diverses maladies des membres qui la composent.  Les transporteurs ABC (voir fig.1) sont présents dans les trois domaines d’organismes vivants  (bactéries, archées et eucaryotes) et constituent décidément la superfamille de protéines la plus vaste. Ainsi, vu leur importance, ils font l’objet de plusieurs études et une bonne disponibilité en masse des données sur ces derniers. Pour bien exploiter ces données, plusieurs méthodes sont disponibles comme le datamining ou fouille de données. La fouille de données est un ensemble de techniques ou méthodes qui permettent d’extraire l’information de grandes bases de données.

![Transporteur ABC](transporteurs.png)
Fig. 1 : Architecture des transporteurs ABC

Pour le compte de ce projet, nous allons appliquer des méthodes de fouille de données sur des données de la banque publique ABCdb (https://www-abcdb.biotoul.fr/) ; celle-ci est développée et maintenue à Toulouse. 
La plupart de ces systèmes sont des transporteurs dont l’architecture est conservée et est composée de 2 domaines pour les exportateurs et 3 domaines pour les importeurs (voir fig.1) . Se basant sur cette architecture de ces domaines, il doit y avoir une possibilité d’identifier de façon automatique et plus efficace l’appartenance à une sous-famille de transporteurs. En d’autres termes, nous supposons qu’il doit y avoir une conservation ou similarité d’architecture entre domaines appartenant à une même sous-famille.

Ainsi, nous allons tenter de répondre à cette question principal :
Est-ce que la présence/absence d’un certain type de domaine fonctionnel peut déterminer l’appartenance à une sous-famille de domaine de transporteurs chez les procaryotes ?

Ainsi nous allons utiliser la méthode de classification pour prédire l’appartenance à une sous-famille de domaine en fonction d’un certain nombre de paramètres en particulier les domaines fonctionnels présents dans le gène. Ensuite, nous allons évaluer la qualité de la prédiction qui a été effectuée. 


# II) Analyse

Le but clair de ce projet est savoir si on peut classifier les famille de transporteurs a partir de la présence ou l’absence de domaine. Pour cela, il faut s'assurer que l'on ait assez d'effectif par sous-famille. Il est intéressant de voir comment sont réparties les sous-familles dans la table Assembly. 

![plot](datavizu.png) 
Fig. 2 : Occurence de chaque sous-famille de transporteur ABC identifiés dans la table Assembly

Ces graphiques sont obtenus à partir de la table Assembly (Functional_Classification). On voit que la répartitions des sous-familles n'est pas homogène, avec des effectifs allant de 1 à 500. Certaines sont mieux représentée que d'autres mais en moyenne leur effectif est de compris entre 10 et 100. Ca peut être un problème pour la classification si on a pas assez d'individus pour représenter une sous-famille. 

Les méthodes disponibles envisageables pour cette classification sont réseau de neurones, arbre de décision, classification bayésienne et la méthode k plus proches voisins. Ce dernier ne sera pas utilisé car sa complexité augmente rapidement avec la taille de l'échantillon. De plus, il donne le même poidS à chaque attribut L'avantage de l'abre de décision est qu'il offre un panel de paramètres tels que la mesure de la qualité ou la méthode de pruning. C'est un plus car nous aurons certainement des sous-familles mal représentées. Le pruning permet de supprimer les parties de l'arbre qui ne semblent pas performantes pour prédire la classe de nouveaux cas. D'un autre coté, le classificateur bayésien requiert peu de données et est rapide. Le réseau de neurone est une méthode très puissante mais demande beaucoup d'entrainement donc du temps et un jeu d'apprentissage conséquent. 

# III) Conception

## Matrice 
On sait sait que les transporteurs ABC sont caractérisés par 3 domaines connus : MSD, SBP et NBD. La matrice finale devra présenter 3 variables correspondant à l'absence ou la présence de ces domaines dans les différents gènes (individus).

### Données utilisées:
Pour réaliser ce projet, on se propose d'utiliser les tables suivantes :
  
- Functionnal_Domain : Cette table comprend les noms des différents domaines portés sur un gène. Parmi les variables, on a Family_link qui permet de faire le lien entre les types de domaines ABC et les sous-familles de transporteurs.
  
- Taxonomy : Cette table de données permet seulement de s'assurer que l'on ne travaille qu’avec des procaryotes et pas des eucaryotes.
  
- Protein et Assembly : Ces deux tables contiennent les Gene_ID des gènes qui sont potentiellement impliqués dans les transporteurs ABC. Ces tables permettent donc de faire le lien entre les domaines et les transporteurs. En effet, Assembly contient les différentes sous-famille de transporteurs qui nous interessent. Aussi, cette table contient Domain_Organization qui permet de voir quels domaines sont présents. La table Protein contient différentes variables que l'on utilise pour nettoyer nos données. 

- Gene : On utilise cette table pour "nommer" nos individus dans la matrice finale à l'aide de l'identifiant des gènes. 

- On utilise les tables Conserved_Domain, Chromosome, Strain pour joindre nos 4 tables d'intérêts.

### Filtrage des données :
Ces données comportent pas mal d'erreur, pour filter les données, on utilise dans la table Protein :
- Identification_Status : on garde les domaines validés par les experts.
- Type : on séléctionne uniquement le type 'ABC'.
- Integrity : on récupere les protéines qui ne sont pas partielles

### Conception de la matrice individus-variables :

Nous voulons prédire  le type de sous-famille de transporteur en fonction de la présence ou l'absence des différents domaines caractérisant un transporteur type ABC( MSD, SBP, NBD), ainsi, notre variable "classe" sera Functionnal_Classification dans la table Assembly.
Pour construire la matrice individus-variables, on a récupéré les variables qui nous intéressent notamment Gene_ID (pour représenter les individus), Domain_Organization, Taxon_Name, Gene_ID, Functional_Classification. A l'aide d'un script R, on créer une variable qui indique quels domaines sont présents dans le transporteurs en fonction de Domain_Organization. Pour cela, on récupere les positions (dans le tableau) des domaines lorsqu'il sont présent, ensemble ou pas et on remplit un vecteur avec le nom du/des domaine-s en question. On obtient un vecteur qui pour chaque ligne indique les domaines présents dans le transporteur. Ce vecteur est la variable Domain_Class que l'on va diviser en 3 variables quantitatives pour chacun des 3 domaines qui indique présence ou absence.

## Classification

On utilise la méthode arbre de décision pour prédire nos sous-familles. C'est une méthode précise, à choisir entre cette méthode ou le classificateur bayésien, on choisit cette méthode car le classificateur bayésien est naif, il fait l'hypothèse de l'indépendance des attributs. pour calculer les probabililtés d'un transporteur d'être d'une classe. C'est un problème car biologiquement, il est fort possible que des domaines soit liés. C'est à dire que la présence de MSD par exemple implique plus souvent la présence de NBD. POur contourner cette difficulté, on utilise arbre de décision.

## Performances

Afin d'évaluer les performances de la méthodes, on utilise une matrice de confusion, dans laquelle on retiendra la précision et le taux d'erreur.

# IV) Réalisation

##       Matrice
 La matrice obtenue compte 9366 lignes et 6 colonnes et est extraite du script R (en format .csv) pour la classification sur Knime.

##       Workflow de Knime
Knime est un logiciel libre et intuitif qui offre une gamme de méthodes de fouille de données à l’aide d’un interface graphique bien cadré. L’interface utilisateur graphique permet la construction de workflow par la jonction bout à bout de nœuds, chacun réalisant une opération spécifique. Chaque nœud correspond à un algorithme bien déterminé.

Nous avons d’abord créé un projet nommé fouille et inséré le nœud CSV READER qui permet de lire et de charger notre jeu de données. Comme établis au dessus, nous avons travaillé sur deux classificateurs à savoir : Arbre de décision (Decision Tree Learner) et Bayésien naïf (Naive Bayes Learner). Nous avons ensuite séparé le jeu donnée en un jeu d’apprentissage et un jeu de test avec l’aide de X-Partitionner Le premier jeu est connecté avec Decision Tree Leaner/ Naive Bayes Leaner ;  il fournit deux mesures de qualité pour le calcul des divisions : l'indice de Gini et le ratio de gain. En outre, il existe une méthode de post-élagage pour réduire la taille de l'arbre et augmenter la précision de la prédiction. Le second jeu de donné est relié à Decision Tree Predictor/ Naive Bayes Predictor ; ce nœud utilise un arbre de décision existant (transmis via le port de modèle déjà créé) pour prédire la valeur de classe des nouveaux modèles. Ce nœud dispose également trois options que l’on peut configurer au besoin. A partir de cette étape on peut bien vérifier et comparer les résultats obtenus entre les deux prédictions pour les deux jeux de données. La sortie de ce dernier nœud est orientée vers celui de X-Aggregator ; il collecte le résultat du nœud précédent,  compare la classe prédite à la classe réelle et génère des prédictions pour toutes les lignes et les taux d’erreur. Le nœud X-Aggregator est relié à Scorer qui fournit entre autres la matrice de confusion et Statistics qui permet une visualisation des résultats.
Notons qu’à chaque niveau, on peut adapter le workflow selon les options disponibles. 
Le workflow ainsi obtenu se présente comme suit.
![plot](Workflow-knime_fouille.png) 

# V) Résultats et Discussion

On a fixé le nombre de validations à dix car on a pu vérifier qu’il n'entraîne pas de différences significatives sur les valeurs de précision obtenues en jouant sur ces valeurs. Nous admettons que les valeurs de Naive Bayesian et celles Naive Bayesian Predictor sont restés à leurs valeurs de défauts. Pour l’arbre de décision, nous avons maintenu les valeurs par défaut et on a varié les sur le type de partitionnement des données (Random Sampling, Linear SAmpling et Stratified Sampling)d’une part et sur la qualité de la mesure (Gain Ratio/Gini index) et méthode d’élagage (MDL/Non pruning) MDL (Minimum Description Length).  Les résultats sont résumés dans ce tableau ci-dessous.


NP: no pruning , *** : donnée partielle utilisée soit les 2000 premières lignes de notre matrice.

![plot](Résultat_KNIME.png)
fig. 3 : Comparaison des taux d'erreurs et de précision entre les deux méthodes de classification utilisées.


Ces résultats montrent que notre modèle ne permet pas de classifier correctement les sous-familles de domaines de transporteurs car les taux d’erreurs sont assez élevés et inversement la précision faible. Néanmoins ils confirment que la classification faite avec l’arbre de décision donne de meilleurs résultats que celle avec la méthode du bayésien naïf. On obtient une précision plus élevée avec MDL et avec Gini index (mesure statistique permettant de rendre compte de la répartition d'une variable au sein d'une population). Le type de partitionnement n’a pas un impact évident sur la précision des prédictions. 
En réglant les paramètres sur “no pruning”, on a pas obtenu de résultats quel que soit le type de partitionnement, nous avons réduit ainsi le jeu de donnée aux  2000 premières lignes pour obtenir un résultat, l’algorithme ne supporte pas l'entrée de donné qui lui est soumis. En comparaison, on a de meilleurs résultats avec la méthode avec MDL malgré la réduction de la quantité de données en entrée qui aété opérée.
  Globalement, la classification avec Bayésien est plus rapide et moins coûteuse en temps mais l’arbre de décision fournit des résultats avec plus de précision. En réduisant de moitié ou du quart l’ensemble de nos données , on constate effectivement que la performance de nos prédictions se retrouve bien améliorée. Ce qui laisse à supposer que le nombre important sous-famille de transporteur est un facteur limitant d’une bonne classification.



# VI) Bilan et perspectives

Au vu de nos résultats, il semble que la classification des sous-familles à partir de la présence ou l'absence des domaines composants les transporteurs ABC n'est pas un succès. En effet, un taux de bonnes prédistions aussi bas n'est pas signe d'une bonne classification. Nous avons plusieurs hypothèses concernant cette échec.

Tout d'abord, on peut se demander si ce n'est pas du au fait que l'on a trop de classes différentes par rapport au variables discriminantes. Le problème peut etre du au filtrage qui est peut être trop souple. Dans l'interêt de réduire le nombre de sous-famille, supprimer les sous-familles avec un effectif inférieur à un seuil peut etre une solution. Ensuite, on peut simplement ajouter des variables supplémentaires, par exemple le nombre de domaine qui caractérise les transporteurs. On voit dans la variable Domain_Organization qui permet de voir l'architecture en domaine des transporteurs le nombre de domaine total. En plus, de la présence/absence des domaines MSD/SBP/NBD, ajouter le nombre de domaine présents pourrait augmenter la précision de la classification (bien que cela modifierait notre problématique originale). De plus, on peut ajouter d'autres variables présentes dans une autre table telle que Protein. Par exemple, Membrane_segment et Signal_Peptide pouraraient aussi s'ajouter à la matrice.
Dans le cas où la méthode de classification est bonne, on peut se demander si notre jeu d'apprentissage est trop petit, auquel cas il faudrait augmenter le nombre de ligne de la matrice. 
Dans un deuxième temps, peut être qu'utiliser une nouvelle méthode plus performante comme un réseau de neurones peut améliorer cette classification. Effectivement, avec une meilleur matrice plus complète, un jeu d'apprentisage plus grand, le réseau de neurone permettrait d'obtenir de meilleurs résultats. Ce dernier permet de travailler sur des données bruitées, donc le fait que les sous-familles soient plus ou moins représentées ne sera pas un problème.

A travers ce projet, nous avons eu à notre disposition une quantité considérable de ressources de documentation, de tutoriel et enfin des données proprements dites. Toutefois, nous avons eu du mal à bien appréhender un sujet et à définir une problématique. Par ailleurs, nous avons eu pas mal de soucis pour concevoir une matrice et à la suite choisir la meileure des méthodes disponibles pour répondre à notre question. En tant que débutant dans ce domaine du datamining, c'est une manière d'expérimenter sur des données réelles de l'importance et de la portée de ces techniques très utile en bioinformatique. Le fait de travailler sur l'environnement gitlab nous a permit de contribuer à tour de rôle sur les tâches qu'on s'est réparties et de mettre en commun de façon plus efficace et simple.

# VII) Gestion du projet

Nous avons commencé par une lecture de l’ensemble des documents mise à notre disposition pour comprendre le but du projet et la fin choisir un sujet. Nos discussions ont été successives et intermittentes pour décider sur un point ou pour se départager des tâches à effectuer. Gitlab a été un atout qui a facilité ce projet.
Les grandes étapes ont été planifiées au début du projet. Des réunions se sont suivies pour tenir au courant de l'avancé du projet.
